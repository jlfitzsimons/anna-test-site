Rails.application.routes.draw do
  get 'pages/home', to: 'pages#home'
  get 'pages/about', to: 'pages#about'
  get 'pages/pricing', to: 'pages#pricing'
  get 'pages/policy', to: 'pages#policy'

  root 'pages#home'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
