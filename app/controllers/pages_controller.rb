class PagesController < ApplicationController
  def home
  end

  def about
  end

  def pricing
  end

  def policy
  end
end
